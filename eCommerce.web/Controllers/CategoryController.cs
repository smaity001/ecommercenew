﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Entity;
using eCommerce.Services;
namespace eCommerce.web.Controllers
{
    public class CategoryController : Controller
    {
        CategoryService categories = null;
        public CategoryController()
        {
            categories = new CategoryService();
        }
        [HttpGet]
        public ActionResult Index()
        {
            var categors = categories.GetCategories();
            return View(categors);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            if(ModelState.IsValid)
            {
                categories.SaveCategory(category);
                ModelState.Clear();
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var cat = categories.GetCategory(ID);
            return View(cat);
        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                categories.UpdateCategory(category);
                ModelState.Clear();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            var cat = categories.GetCategory(ID);
            return View(cat);
        }
        [HttpPost]
        public ActionResult Delete(int ID,Category category)
        {
            if (ModelState.IsValid)
            {
                categories.DeleteCategory(ID,category);
            }
            return RedirectToAction("Index");
        }
    }
}