﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Entity;
using eCommerce.Services;
namespace eCommerce.web.Controllers
{
    public class ProductController : Controller
    {
        ProductService productservice = null;
        public ProductController()
        {
            productservice = new ProductService();
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ProductTable()
        {
            var products = productservice.GetProducts();
            return PartialView(products);
        }
        [HttpPost]
        public ActionResult ProductTable(string search)
        {
            var products = productservice.GetProduct(search);
            return PartialView(products);
        }
        [HttpGet]
        public ActionResult Create()
        {    
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(Product product)
        {
            if(ModelState.IsValid)
            {
                productservice.SaveProduct(product);
                ModelState.Clear();
            }            
            return RedirectToAction("Index");
        }
    }
}