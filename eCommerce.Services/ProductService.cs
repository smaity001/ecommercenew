﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Entity;
using eCommerce.Database;

namespace eCommerce.Services
{
    public class ProductService
    {
        public List<Product> GetProducts()
        {
            using (var context = new eCommerceDBContext())
            {
                var products = context.Products.ToList();
                return products;
            }
        }
        public void SaveProduct(Product product)
        {
            using (var context = new eCommerceDBContext())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }
        public Product GetProduct(int ID)
        {
            using (var context = new eCommerceDBContext())
            {
                var product = context.Products.FirstOrDefault(m=>m.ID==ID);
                return product;
            }
        }
        public List<Product> GetProduct(string search)
        {
            using (var context = new eCommerceDBContext())
            {
                var product = context.Products.Where(m => m.Name == search).ToList();
                return product;
            }
        }
        public void UpdateProduct(Product product)
        {
            using (var context = new eCommerceDBContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteProduct(int ID,Product product)
        {
            using (var context = new eCommerceDBContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}