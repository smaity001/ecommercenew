﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Entity;
using eCommerce.Database;

namespace eCommerce.Services
{
    public class CategoryService
    {
        public List<Category> GetCategories()
        {
            using (var context = new eCommerceDBContext())
            {
                var categories = context.Categories.ToList();
                return categories;
            }
        }
        public void SaveCategory(Category category)
        {
            using (var context = new eCommerceDBContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public Category GetCategory(int ID)
        {
            using (var context = new eCommerceDBContext())
            {
                var categories = context.Categories.FirstOrDefault(m=>m.ID==ID);
                return categories;
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new eCommerceDBContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteCategory(int ID,Category category)
        {
            using (var context = new eCommerceDBContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}